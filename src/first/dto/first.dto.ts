import { ApiProperty } from '@nestjs/swagger';
export class FirstDto {
    @ApiProperty({
        description: 'version',
        default: "",
        required: false
    })
    version: string;
}
