import { Controller, Get, Query, Res, Version, VERSION_NEUTRAL } from '@nestjs/common';
import { ApiHeader } from '@nestjs/swagger';
import { FirstServiceV1 } from './first.serviceV1';

@Controller({path:'hello',version:'1'}) 
export class FirstControllerV1 {
  constructor(private readonly firstServiceV1: FirstServiceV1) {}
  @Get('world')
  async getHello() {
    let res = {};
    res['status'] = 'success';
    res['message'] = this.firstServiceV1.getHello();
    return res;
  }
}

