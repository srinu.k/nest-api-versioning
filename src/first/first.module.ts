import { Module } from "@nestjs/common";
import { FirstController } from "./first.controller";
import { FirstService } from "./first.service";
import { FirstControllerV1 } from "./V1/first.controllerV1";
import { FirstServiceV1 } from "./V1/first.serviceV1";

@Module({
    imports:[],
    controllers: [FirstController,FirstControllerV1],
    providers: [FirstService,FirstServiceV1],
    exports:[]
  })
  export class FirstModule {}