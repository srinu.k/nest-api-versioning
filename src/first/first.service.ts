import { Injectable } from '@nestjs/common';

@Injectable()
export class FirstService {
  getHello(): string {
    return 'I Am Version 1';
  }
}
