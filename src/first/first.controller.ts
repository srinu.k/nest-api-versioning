import { Controller, Get, Query, Res, Version, VERSION_NEUTRAL,Headers } from '@nestjs/common';
import { ApiHeader } from '@nestjs/swagger';
import { FirstDto } from './dto/first.dto';
import { FirstService } from './first.service';

// general

@Controller({path:'hello'}) 
export class FirstController {
  constructor(private readonly appService: FirstService) {}
  @Get('world')
  getHello() {
    let res = {};
    res['status'] = 'success';
    res['message'] = this.appService.getHello();
    return res;
  }
} 

// param versioning 

// @Controller({path:'hello'}) 
// export class FirstController {
//   @Get('world')
//   async getHello(@Query() query: FirstDto) {
//     let res = {};
//       const version = query.version
//       res['status'] = 'success';
//       res['message'] = 'I Am Version 1'
//       if(version == 'v1'){
//         res['message'] = 'I Am Version 2'
//       }
//       return res;
//   }
// }

