import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppTwoController } from './app.two.controller';
import { FirstModule } from './first/first.module';

@Module({
  imports: [
    FirstModule,
    RouterModule.register([
    {
      path: 'first',
      module: FirstModule,
    },
  ]),
],
  // controllers: [AppController],
  //URI VERSIONING WITH 2 CONTROLLERS
  // controllers: [AppController,AppTwoController],

  //URI VERSIONING WITH 1 CONTROLLERS
  controllers: [],
  providers: [],
})
export class AppModule {}
