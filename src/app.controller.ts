import { Controller, Get, Version, VERSION_NEUTRAL } from '@nestjs/common';
import { ApiHeader } from '@nestjs/swagger';
import { AppService } from './app.service';

//URI VERSIONING WITH TWO CONTROLLER
// @Controller({path:'hello',version:'1'}) 
// export class AppController {
//   constructor(private readonly appService: AppService) {}
//   @Get('world')
//   getHello(): string {
//     return this.appService.getHello();
//   }
// } 

//URI VERSIONING WITH ONE CONTROLLER
// @Controller({path:'hello'}) 
// export class AppController {
//   constructor(private readonly appService: AppService) {}
//   @Get('world')
//   @Version('1')
//   getHello(): string {
//     return this.appService.getHello();
//   }
//   @Get('world')
//   @Version('2')
//   getHello2(): string {
//     return 'I Am Version 2';
//   }
// } 

//HEADER VERSIONING
@Controller({path:'hello'}) 
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Get('world')
  @Version('1')
  getHello(): string {
    return this.appService.getHello();
  }
  @ApiHeader({name:'Version',enum:['1','2'],description:'select version',required : true})
  @Get('world')
  @Version('2')
  getHello2(): string {
    return 'I Am Version 2';
  }
} 
