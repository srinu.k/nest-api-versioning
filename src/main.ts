import { VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerDocumentOptions, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  //URI VERSIONING
  app.enableVersioning({
    type:VersioningType.URI,
  })

  //HEADER VERSIONING
  // app.enableVersioning({
  //   type:VersioningType.HEADER,
  //   header:'Version'
  // })
  const config = new DocumentBuilder().setTitle('Demo Application')
                      .setDescription("Demo API Application")
                      .setVersion('v1')
                      .addTag('API VERSIONING')
                      .addBearerAuth(
                        { type: 'http', in: 'header' },
                        )
                      .build();
  const options: SwaggerDocumentOptions = {
      deepScanRoutes: true
  };
  const document = SwaggerModule.createDocument(app, config,options);
  SwaggerModule.setup('/', app, document);

  await app.listen(3000);
}
bootstrap();  
