# Neo Api Versioning

Purplle Neo Api Versioning
​
## Table of Contents
1. [Setup](#setup)
    - [Git Congifuration](#git-configuration)
    - [Clone the Repository](#clone-the-repository)
    - [Project Configuration](#project-configuration)
​
2. [Running the Project](#running-the-project)
​
3. [Development](#development)
​
4. [Components](#components)
​
5. [Technologies](#technologies)
​
## Setup
### Git Configuration
​
Configure Git on your local machine (Make sure Git is installed)
​
```bash
$ git config --global user.name "your_username"
$ git config --global user.email "your_email_address@purplle.com"
```
​
### Clone the Repository
​
Start by cloning this project on your workstation.

Clone with SSH (when you want to authenticate only one time)
```bash
$ git clone git@gitlab.com:srinu.k/nest-api-versioning.git
```
​
Clone with HTTPS (when you want to authenticate each time you perform an operation between your computer and GitLab)
```bash
$ git clone https://gitlab.com/srinu.k/nest-api-versioning.git
```
​
### Project Configuration
​
To view the files,
```bash
$ cd nest-api-versioning
```
​
The next thing will be to install all the dependencies of the project.

To install required packages,
```bash
$ npm install
```
​
## Running the Project
```
$ nest start --watch
```

## Components
1. URI VERSIONIG 
    - Uncomment the first method in first.controller.ts file(if commented)
    - run the project
​
2. Param Versioning
    - Uncomment the second method in first.controller.ts file
    - run the project
​
## Technologies
1. NestJS [(Documentation)](https://docs.nestjs.com/ "Documentation | NestJS - A progressive Node.js framework")
2. TypeScript [(Documentation)](https://www.typescriptlang.org/docs/ "TypeScript: The starting point for learning TypeScript")